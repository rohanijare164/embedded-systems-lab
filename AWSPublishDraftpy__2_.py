#AWSPublishDraft.py
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import logging
import time
import argparse
import json

AllowedActions = ['both', 'publish', 'subscribe']


# Custom MQTT message callback
def customCallback(client, userdata, message):
    print("Received a new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")


host = 'albetoygflax4-ats.iot.us-east-2.amazonaws.com'
rootCAPath = 'root-ca-cert.pem'
certificatePath = '2e779ee87c.cert.pem'
privateKeyPath = '2e779ee87c.private.key'
clientId = 'camera'
thingName = 'SubscriberG07'
topic = 'esp32/rover'
topic1 = 'esp32/target'
port=8883
useWebsocket=False
mode='subscribe' #CHANGE: change hero to other allowed actions

	
	


if mode not in AllowedActions:
	print ("Please select a valid mode")
	exit(2)

if useWebsocket and certificatePath and privateKeyPath:
    print("X.509 cert authentication and WebSocket are mutual exclusive. Please pick one.")
    exit(2)

if not useWebsocket and (not certificatePath or not privateKeyPath):
    print("Missing credentials for authentication.")
    exit(2)

# Port defaults
if useWebsocket:  # When no port override for WebSocket, default to 443
    port = 443
if not useWebsocket:  # When no port override for non-WebSocket, default to 8883
    port = 8883
	
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#Comment this part to avoid logger information to be printed on the screen
# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

# Init AWSIoTMQTTClient
myAWSIoTMQTTClient = None
if useWebsocket:
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId, useWebsocket=True)
    myAWSIoTMQTTClient.configureEndpoint(host, port)
    myAWSIoTMQTTClient.configureCredentials(rootCAPath)
else:
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
    myAWSIoTMQTTClient.configureEndpoint(host, port)
    myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect()
if mode == 'both' or mode == 'subscribe':
    myAWSIoTMQTTClient.subscribe(topic, 1, customCallback)
    myAWSIoTMQTTClient.subscribe(topic1, 1, customCallback)
time.sleep(2)

# Publish to the same topic in a loop forever
loopCount = 0
while True:
	if mode == 'both' or mode == 'publish':

		mes = json.dumps({'test': 'data: {}'.format(loopCount)})		
		myAWSIoTMQTTClient.publish(topic, mes, 1)	



		
		if mode == 'publish':
			print('Published topic %s: %s\n' % (topic, mes))
		loopCount += 1
	time.sleep(1)