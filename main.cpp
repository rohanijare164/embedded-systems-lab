#include <Arduino.h>
#include <OPT3101.h>
#include <Wire.h>
#include "sensorDriver.h"
#include "motorDriver.h"

void taskOne( void * parameter);
void taskTwo( void * parameter);
     int L,R,M;
#define LED_BOARD 2 //change here the pin of the board to V2


void setup(){
  pinMode(LED_BOARD, OUTPUT);
  Serial.begin(9600);
  sensorobject.SETUP();
  motorobject.SETUP();

TaskHandle_t TaskOne;
TaskHandle_t TaskTwo;
  
  delay(1000);
  xTaskCreate(
                   taskOne,          /* Task function. */
                    "TaskOne",        /* String with name of task. */
                    1024,              /* Stack size in bytes. */
                   NULL,             /* Parameter passed as input of the task */
                    2,                /* Priority of the task. */
                    &TaskOne);           /* Task handle to keep track of created task */   
                   //   0);               /* Pin task to core 0 */  
                  //NULL);            /* Task handle. */

  xTaskCreate(
                    taskTwo,          /* Task function. */
                    "TaskTwo",        /* String with name of task. */
                    1024,              /* Stack size in bytes. */
                    NULL,             /* Parameter passed as input of the task */
                    1,                /* Priority of the task. */
                    &TaskTwo);          /* Task handle to keep track of created task */   
                    //  1);               /* Pin task to core 1 */
                    
                 //   NULL);            /* Task handle. */
    
}

void loop(){
delay(1000);
}

void taskOne( void * parameter )
{
   
      int16_t *arr;
    //example of a task that executes for some time and then is deleted
    for( ; ;  )
    {
      Serial.println("Reading sensor data");
      arr = sensorobject.reading();
      Serial.println(*arr);
      L =*arr;
      Serial.println(*(arr+1));
      M = *arr+1;
      Serial.println(*(arr+2));
      //Serial.println(M);
      R= *arr+2;

      //Switch on the LED
      digitalWrite(LED_BOARD, HIGH); 
      //delay(500); //This delay doesn't give a chance to the other tasks to execute
      //digitalWrite(LED_BOARD, LOW);

      vTaskDelay(100 / portTICK_PERIOD_MS);
      
      //Switch on the LED
      //digitalWrite(LED_BOARD, HIGH); 
      // Pause the task for 1000ms
      //delay(1000); //This delay doesn't give a chance to the other tasks to execute
      //vTaskDelay(1000 / portTICK_PERIOD_MS); //this pauses the task, so others can execute
      // Switch off the LED
      //digitalWrite(LED_BOARD, LOW);
      // Pause the task again for 500ms
      //vTaskDelay(500 / portTICK_PERIOD_MS);
    }
    //Serial.println("Ending task: 1");
    //vTaskDelete( NULL );
}
 
void taskTwo( void * parameter)
{
    //create an endless loop so the task executes forever

     //motorobject.motor_direction(MotorA,Forward);
     //motorobject.motor_direction(MotorB,Backward);

    //motorobject.set_speed(MotorA,Forward,200);
   // motorobject.set_speed(MotorB,Backward,200); 

   //Switch on the LED
   //int x =0;
   //if(x%5==0)
   //{ 
   //digitalWrite(LED_BOARD, !x); 
   //x++;
   //}
   
 Serial.println("Hello from task: 2");
      for( ;; )
    {
        
        if(M<250)
        
        { 
          if(M<100)
          {
          motorobject.set_speed(MotorA,Forward,0);
          motorobject.set_speed(MotorB,Forward,0);
          vTaskDelay( 100/ portTICK_PERIOD_MS);
          }          
         else if(L>300)
          {
          motorobject.set_speed(MotorA,Backward,80);
          motorobject.set_speed(MotorB,Backward,80); 
          vTaskDelay(100 / portTICK_PERIOD_MS);
          }
          else
          {
           motorobject.set_speed(MotorA,Forward,100);
          motorobject.set_speed(MotorB,Forward,100);
          }
        }
          else if (R<300)
          {
            motorobject.set_speed(MotorA,Backward,80);
            motorobject.set_speed(MotorB,Backward,80); 
            vTaskDelay(100 / portTICK_PERIOD_MS);
          }
          else if (L<300)
          {
            motorobject.set_speed(MotorA,Forward,100);
          motorobject.set_speed(MotorB,Forward,100);
          }       
        else
        {
            motorobject.set_speed(MotorA,Forward,80);
            motorobject.set_speed(MotorB,Backward,80); 
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }
       digitalWrite(LED_BOARD,LOW); 
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
    //Serial.println("Ending task 2"); //should not reach this point but in case...
    //vTaskDelete( NULL );
}
